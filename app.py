from todo_website import create_app

app = create_app()

if "__main__" == __name__:
    app.run(host="0.0.0.0", debug=True)
