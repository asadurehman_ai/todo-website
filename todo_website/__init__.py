from flask import Flask
from .extns import db

from .user import user
from .api import api


def create_app():
    app = Flask(__name__)

    app.config['SECRET_KEY'] = "123321"
    app.config['SQLALCHEMY_DATABASE_URI'] = "mysql://reqsols_todo:reqsols123@www.db4free.net:3306/todo_website"

    # BLUEPRINT REGISTRATION
    app.register_blueprint(user)
    app.register_blueprint(api)
    db.init_app(app)

    return app