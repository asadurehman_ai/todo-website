from .extns import db
from flask import session, url_for


class StickyNote(db.Model):

    """
    Sticky Note Table takes
    title: String -> Maximum Length char(225)
    content: Text
    color_id: Integer -> Foreign Key with "Color" Table
    list_id: Integer -> Foreign Key with "List" Table

    color: SQLAlchemy-Relationship -> Gets the <Color Object> for that sticky note

    """
    __tablename__ = "stickynote"
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(255), nullable=False)
    content = db.Column(db.Text, nullable=False)

    color_id = db.Column(db.Integer, db.ForeignKey("color.id"))
    list_id = db.Column(db.Integer, db.ForeignKey("list.id"))
    user_id = db.Column(db.Integer, db.ForeignKey("user.id"))

    def save_to_database(self):
        db.session.add(self)
        db.session.commit()

    def delete_from_database(self):
        db.session.delete(self)
        db.session.commit()

    def __repr__(self):
        return f"<StickyNote: {self.title}>"


class Color(db.Model):
    """
        Color Table takes
        color: String -> Maximum Length char(255)
        sticky_note: SQLAlchemy-Relationship -> Gets array <Sticky Note Objects> for that color
    """

    __tablename__ = "color"
    id = db.Column(db.Integer, primary_key=True)
    color = db.Column(db.String(255), nullable=False)

    sticky_note = db.relationship("StickyNote", backref="color")

    def __repr__(self):
        return f"<Color: {self.color}>"


class List(db.Model):
    __tablename__ = "list"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50), nullable=False)
    color_id = db.Column(db.Integer, db.ForeignKey("color.id"))
    user_id = db.Column(db.Integer, db.ForeignKey("user.id"))

    color = db.relationship("Color", backref="backref_list")
    sticky_note = db.relationship("StickyNote", backref="list")
    user = db.relationship("User", backref="list")

    def save_to_database(self):
        db.session.add(self)
        db.session.commit()

    def delete_from_database(self):
        db.session.delete(self)
        db.session.commit()

    def __repr__(self):
        return f"<List: {self.name}>"


class User(db.Model):
    __tablename__ = "user"
    id = db.Column(db.Integer, primary_key=True)
    full_name = db.Column(db.String(255), nullable=False)
    username = db.Column(db.String(25), nullable=False)
    password = db.Column(db.String(255), nullable=False)
    profile_picture = db.Column(db.String(255))

    sticky_notes = db.relationship("StickyNote", backref="user")

    def save_to_database(self):
        db.session.add(self)
        db.session.commit()

    def delete_from_database(self):
        db.session.delete(self)
        db.session.commit()

    def add_to_session(self):
        session['user_profile'] = self.profile_picture
        session['full_name'] = self.full_name
        session['user_id'] = self.id

    def __repr__(self):
        return f"<User: {self.full_name}>"