from flask import Blueprint, render_template, request, redirect, url_for, session, abort
from todo_website.model import StickyNote, Color, List
from todo_website.extns import db
from todo_website.utils import check_user_login

sticky_wall = Blueprint("sticky_wall", __name__)


@sticky_wall.before_request
def login_check():
    login_state = check_user_login(is_login=False, url="user.auth.login")
    if login_state:
        return login_state


@sticky_wall.route("/")
def index():
    # FILHAL SOLUTION
    sticky_notes = StickyNote.query.filter(StickyNote.user_id==session['user_id']).all()
    sticky_note_colors = Color.query.all()
    all_lists = List.query.filter(List.user_id==session['user_id']).all()

    return render_template("sticky_wall.html"
                           , title="Sticky Wall"
                           , sticky_notes=sticky_notes
                           , sticky_note_colors=sticky_note_colors
                           , all_lists=all_lists)


@sticky_wall.route("/add", methods=['POST'])
def add():
    data = request.form.to_dict(flat=True)

    sticky_note = StickyNote(title=data['sticky-note-title']
                             , content=data['sticky-note-content']
                             , color_id=data['sticky-note-color']
                             , list_id=data['sticky-note-list']
                             , user_id=session['user_id'])

    sticky_note.save_to_database()

    return redirect(url_for("user.sticky_wall.index"))


@sticky_wall.route("/delete/<sticky_note_id>")
def delete(sticky_note_id):
    sticky_note = StickyNote.query.get_or_404(sticky_note_id)
    if sticky_note.user_id != session['user_id']:
        return abort(403)

    sticky_note.delete_from_database()
    return redirect(url_for("user.sticky_wall.index"))


@sticky_wall.route("/update/<sticky_note_id>", methods=['GET', 'POST'])
def update(sticky_note_id):

    sticky_note = StickyNote.query.get_or_404(sticky_note_id)
    if sticky_note.user_id != session['user_id']:
        return abort(403)

    if request.method == "GET":
        colors = Color.query.all()

    if request.method == "POST":
        data = request.form.to_dict(flat=True)

        sticky_note.title = data['title']
        sticky_note.content = data['content']
        sticky_note.color_id = data['color']

        db.session.commit()

        return redirect(url_for("user.sticky_wall.index"))

    return render_template("sticky_note_update.html"
                           , colors=colors
                           , sticky_note=sticky_note)