from flask import Blueprint, render_template, request, redirect, url_for, session, abort
from todo_website.model import StickyNote, Color, List
from todo_website.utils import check_user_login

lists = Blueprint("lists", __name__, url_prefix="/lists")


@lists.before_request
def login_check():
    login_state = check_user_login(is_login=False, url="auth.login")
    if login_state:
        return login_state


@lists.route("/<list_id>")
def index(list_id):

    # CHECK IF THE USER IS ACCESSING HIS OWN LIST
    list = List.query.get_or_404(list_id)

    if list.user_id != session['user_id']:
        return abort(403)

    all_lists = List.query.filter(List.user_id==session['user_id']).all()

    sticky_note_colors = Color.query.all()

    return render_template("sticky_wall.html"
                           , title=list.name + " Lists"
                           , all_lists=all_lists
                           , sticky_notes=list.sticky_note
                           , sticky_note_colors=sticky_note_colors
                           , list=list)


@lists.route("/add/<list_id>", methods=['POST'])
def add_sticky_note(list_id):

    lists = List.query.get_or_404(list_id)
    if lists.user_id != session['user_id']:
        return abort(403)

    data = request.form.to_dict(flat=True)

    note = StickyNote(title=data['sticky-note-title']
                      , content=data['sticky-note-content']
                      , color_id=data['sticky-note-color']
                      , list_id=list_id)

    note.save_to_database()

    return redirect(url_for("user.lists.index", list_id=list_id))


@lists.route("/add-list", methods=['POST'])
def add_list():
    data = request.form.to_dict(flat=True)

    new_list = List(name=data['list-title']
                    , color_id=data['list-color']
                    , user_id=session['user_id'])

    new_list.save_to_database()

    return redirect(url_for("user.sticky_wall.index"))


@lists.route("/delete/<list_id>")
def delete_list(list_id):
    delete_list = List.query.get_or_404(list_id)

    if delete_list.user_id != session['user_id']:
        return abort(403)

    delete_list.delete_from_database()

    return redirect(url_for("user.sticky_wall.index"))