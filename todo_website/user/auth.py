from flask import Blueprint, render_template, request, redirect, url_for, session
from todo_website.model import User
from todo_website.utils import save_profile_picture, check_user_login

auth = Blueprint("auth", __name__, url_prefix="/auth")


@auth.route("/register", methods=['GET', 'POST'])
def register():

    login_state = check_user_login(is_login=True, url="sticky_wall.index")
    if login_state:
        return login_state

    if request.method == "POST":
        data = request.form.to_dict(flat=True)

        if User.query.filter(User.username==data['username']).first():
            # SOME ERROR MESSAGE
            return redirect(url_for("user.auth.register"))

        user = User(full_name=data['fullname']
                    , username=data['username']
                    , password=data['password'] # ENCRYPT --> 123 - hs823bdajshd28dhakscnksd
                    , profile_picture=url_for("static", filename="images/default.jpg", _external=True))

        if 'profile-pic' in request.files:
            file = request.files['profile-pic']
            filename = save_profile_picture(file)
            user.profile_picture = url_for("static", filename="images/"+filename, _external=True)

        user.save_to_database()
        user.add_to_session()
        return redirect(url_for("user.sticky_wall.index"))

    return render_template("register.html")


@auth.route("/login", methods=['GET', 'POST'])
def login():

    login_state = check_user_login(is_login=True, url="sticky_wall.index")
    if login_state:
        return login_state

    if request.method == "POST":
        data = request.form.to_dict(flat=True)

        user = User.query.filter(User.username==data['username']).first()

        if not user:
            # SOME ERROR
            return redirect(url_for("user.auth.login"))

        if user.password == data['password']:
            user.add_to_session()
            return redirect(url_for("user.sticky_wall.index"))

    return render_template("login.html")


@auth.route("/logout")
def logout():
    if "user_id" in session:
        session.pop("full_name")
        session.pop("user_id")
        session.pop("user_profile")

        return redirect(url_for("user.auth.login"))

    return redirect(url_for("user.sticky_wall.index"))