from flask import Blueprint

from .auth import auth
from .sticky_wall import sticky_wall
from .lists import lists
from .search import search

user = Blueprint("user", __name__)

user.register_blueprint(auth)
user.register_blueprint(sticky_wall)
user.register_blueprint(lists)
user.register_blueprint(search)