from flask import Blueprint, render_template, request
from todo_website.model import StickyNote, Color
from todo_website.utils import check_user_login

search = Blueprint("search", __name__, url_prefix="/search")


@search.before_request
def login_check():
    login_state = check_user_login(is_login=False, url="auth.login")
    if login_state:
        return login_state


@search.route("/")
def index():

    search = request.args.get("search", None, type=str)

    result_sticky_note = StickyNote.query.filter(StickyNote.title.like(f"%{search}%")).all()
    result_todos = ""

    return render_template("search-result.html"
                           , title=f"Search Results for {search.capitalize()}"
                           , sticky_notes=result_sticky_note
                           , todos=result_todos)