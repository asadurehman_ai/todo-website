from flask import session, url_for, redirect, flash
import os
import uuid


def save_profile_picture(file):
    UPLOADS_PATH = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'static\\images')

    file_extenstion = file.filename.split(".")[-1]
    filename = f"{uuid.uuid4()}.{file_extenstion}"

    file.save(os.path.join(UPLOADS_PATH, filename))

    return filename


def check_user_login(is_login: bool, url: str):
    if is_login:
        # CHECKS FOR LOGIN AND REGISTER
        if "user_id" in session:
            return redirect(url_for(url))
    else:
        # STICKY_WALL, LISTS....
        if "user_id" not in session:
            flash("Login to Continue Using Website!")
            return redirect(url_for(url)) #/auth/login

    return False