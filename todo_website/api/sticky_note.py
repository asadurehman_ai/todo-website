from flask import Blueprint
from todo_website.extns import db
from todo_website.model import StickyNote

sticky_note = Blueprint("sticky_note", __name__, url_prefix="/sticky_note")


@sticky_note.route("/", methods=['GET'])
def get_all_sticky_notes():

    # WE WILL ACCESS HEADERS
    # USER_ACCESS_TOKEN = user.id or apna bana len

    sticky_notes = StickyNote.query.all()

    response = dict()
    if sticky_notes:
        response = {
            "state": "SUCCESS",
            "code": "STICKY_NOTE_FOUND",
            "message": ""
        }
        response['result'] = list()
        for sn in sticky_notes:
            response['result'].append(
                {
                    "id": sn.id,
                    "title": sn.title,
                    "content": sn.content,
                    "color": sn.color.color,
                    "list": {
                        "id": sn.list.id,
                        "name": sn.list.name,
                        "color": sn.list.color.color
                    },
                    "user": {
                        "name": sn.user.full_name
                    }
                }
            )
    else:
        response = {
                "state": "SUCCESS",
                "message": "No Sticky Notes Found for this user!",
                "code": "NO_STICKY_NOTE_FOUND"
            }

    return response