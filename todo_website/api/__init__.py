from flask import Blueprint

from .sticky_note import sticky_note

api = Blueprint("api", __name__, url_prefix="/api")

# REGISTER API's BLUEPRINTS
api.register_blueprint(sticky_note)